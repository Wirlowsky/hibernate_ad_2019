/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package APP2;

import conection.NewHibernateUtil;
import modelo2_2.Profesor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Rafael
 */
public class AppProfesor2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SessionFactory factory = NewHibernateUtil.getSessionFactory();
        Session session = NewHibernateUtil.getCurrentSession();

        // CREAMOS UN OBJETO
        Profesor profesor = new Profesor(8, "Pepe", "Garcia", "Perez");
        Profesor profesor2 = new Profesor(5, "Gallego", "Folla", "Cabras");
        session.beginTransaction();

        //GUARDAR OBJETO
        session.saveOrUpdate(profesor);
        session.saveOrUpdate(profesor2);
        
         //CERRAR CONEXION
        session.getTransaction().commit();
        NewHibernateUtil.CloseSession();

    }

}

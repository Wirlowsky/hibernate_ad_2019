/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conection;

import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Rafael
 */
public class NewHibernateUtil {

    private static final SessionFactory sessionFactory;
    private static final ThreadLocal session=new ThreadLocal();
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            //sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
            Configuration config= new Configuration().configure();
            sessionFactory=config.buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static Session getCurrentSession(){
    Session s = (Session) session.get();
    if(s==null||!s.isOpen()){
    s= sessionFactory.openSession();
    }
    session.set(s);
    return s;
    }
    
    
    public static void CloseSession(){
    Session s= (Session) session.get();
    if(s!=null|| s.isOpen()){
    s.close();
    }
    session.set(s);
    }
}
